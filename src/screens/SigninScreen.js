import React, { useContext } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Context as AuthContext } from '../context/AuthContext';
import { NavigationEvents } from "react-navigation";
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';

const SigninScreen = ({ navigation }) => {
    const { state, signin, clearErrorMessage } = useContext(AuthContext);

    return (
        <View style={styles.container}>
        <NavigationEvents 
            onWillBlur={() => { clearErrorMessage() }}
        />
        <AuthForm 
            headerText='Sign in to your Account'
            errorMessage={state.errorMessage}
            submitButtonText='Sign in'
            onSubmit={({email, password}) => signin({email, password})}
        />
        <NavLink 
            text='Dont have account yet? Sign up instead'
            routeName='Signup'
        />
    </View>
    );
};

SigninScreen.navigationOptions = () => {
    return {
        header: null
    };
};

const styles = StyleSheet.create({
    container:{
        flex : 1,
        justifyContent: 'center',
        marginBottom: 100
    }
});

export default SigninScreen;
