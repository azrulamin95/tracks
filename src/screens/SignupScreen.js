import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Context as AuthContext } from '../context/AuthContext';
import { NavigationEvents } from "react-navigation";
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';

const SignupScreen = ({ navigation }) => {
    const { state, signup, clearErrorMessage } = useContext(AuthContext);
    
    return (
        <View style={styles.container}>
            <NavigationEvents 
                onWillBlur={() => { clearErrorMessage() }}
            />
            <AuthForm 
                headerText='Sign Up for Tracker'
                errorMessage={state.errorMessage}
                submitButtonText='Sign Up'
                onSubmit={({email, password}) => signup({email, password})}
            />
            <NavLink 
                text='Already have an account? Sign in instead'
                routeName='Signin'
            />
        </View>
    );
};

SignupScreen.navigationOptions = () => {
    return {
        header: null
    };
};

const styles = StyleSheet.create({
    container:{
        flex : 1,
        justifyContent: 'center',
        marginBottom: 100
    }
});

export default SignupScreen;
